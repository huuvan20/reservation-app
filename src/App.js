import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import MainContent from './MainContent';

class App extends Component {

  state = {
    isFiltered: false,
    pendingGuest: "",
    guests: [
      {
        name: 'Treasure',
        isConfirmed: false,
        isEditing: false
      },
      {
        name: 'Nic',
        isConfirmed: true,
        isEditing: false
      },
      {
        name: 'Matt K',
        isConfirmed: false,
        isEditing: false
      }
    ]
  }

  toggleGuessPropertyAt = (property, indexToChange) =>
    this.setState({
      guests: this.state.guests.map((guest, index) => {
        if (index === indexToChange) {
          return {
            ...guest,
            [property]: !guest[property]
          }
        }
        return guest;
      })
    });

  toggleConfirmationAt = index =>
    this.toggleGuessPropertyAt('isConfirmed', index);

  toggleEditingAt = index =>
    this.toggleGuessPropertyAt('isEditing', index);

  setNameAt = (name, indexToChange) =>
    this.setState({
      guests: this.state.guests.map((guest, index) => {
        if (index === indexToChange) {
          return {
            ...guest,
            name
          }
        }
        return guest;
      })
    });

  toggleFilter = () =>
    this.setState({ isFiltered: !this.state.isFiltered });

  addGuestName = name =>
    this.setState({
      pendingGuest: name
    });

  submitGuest = e => {
    e.preventDefault();
    this.setState({
      guests: [
        {
          name: this.state.pendingGuest,
          isConfirmed: false,
          isEditing: false
        },
        ...this.state.guests
      ],
      pendingGuest: ""
    });
  }

  removeGuestAt = (index) =>
    this.setState({
      guests: [
        ...this.state.guests.slice(0, index),
        ...this.state.guests.slice(index + 1)
      ]
    });

  getTotalInvited = () => this.state.guests.length;
  getAttendingGuests = () => this.state.guests.reduce(
    (total, guest) => guest.isConfirmed ? total + 1 : total,
    0
  );

  render() {

    const numberTotal = this.getTotalInvited();
    const numberAttending = this.getAttendingGuests();
    const numberUnconfirmed = numberTotal - numberAttending;

    return (
      <div className="App">

        <Header
          submitGuest={this.submitGuest}
          pendingGuest={this.state.pendingGuest}
          addGuestName={this.addGuestName} />

        <MainContent
          pendingGuest={this.state.pendingGuest}
          toggleFilter={this.toggleFilter}
          isFiltered={this.state.isFiltered}
          numberTotal={numberTotal}
          numberAttending={numberAttending}
          numberUnconfirmed={numberUnconfirmed}
          guests={this.state.guests}
          toggleConfirmationAt={this.toggleConfirmationAt}
          toggleEditingAt={this.toggleEditingAt}
          setNameAt={this.setNameAt}
          removeGuestAt={this.removeGuestAt} />

      </div>
    );
  }
}

export default App;
