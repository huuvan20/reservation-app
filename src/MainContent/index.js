import React from 'react';
import PropTypes from 'prop-types';
import GuestList from './GuestList';
import Counter from './Counter';
import ConfirmedFilter from './ConfirmedFilter';

const MainContent = props =>
  <div className="main">
    <div>
      <h2>Invitees</h2>
      <ConfirmedFilter
        toggleFilter={props.toggleFilter}
        isFiltered={props.isFiltered} />
    </div>
    <Counter
      numberTotal={props.numberTotal}
      numberAttending={props.numberAttending}
      numberUnconfirmed={props.numberUnconfirmed} 
    />
    <GuestList 
      guests={props.guests} 
      toggleConfirmationAt={props.toggleConfirmationAt}
      toggleEditingAt={props.toggleEditingAt}
      setNameAt={props.setNameAt}
      isFiltered={props.isFiltered}
      removeGuestAt={props.removeGuestAt}
      pendingGuest={props.pendingGuest}
    />
  </div>

MainContent.propTypes = {
  toggleFilter: PropTypes.func.isRequired,
  isFiltered: PropTypes.bool.isRequired,
  guests: PropTypes.array.isRequired,
  toggleConfirmationAt: PropTypes.func.isRequired,
  toggleEditingAt: PropTypes.func.isRequired,
  setNameAt: PropTypes.func.isRequired,
  removeGuestAt: PropTypes.func.isRequired,
  pendingGuest: PropTypes.string.isRequired,
  numberTotal: PropTypes.number,
  numberAttending: PropTypes.number,
  numberUnconfirmed: PropTypes.number
}

export default MainContent;