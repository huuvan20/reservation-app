import React from 'react';
import PropTypes from 'prop-types';
import GuestInputForm from './GuestInputForm';

const Header = props => 
  <header>
    <h1>RSVP</h1>
    <GuestInputForm
      submitGuest={props.submitGuest}
      pendingGuest={props.pendingGuest}
      addGuestName={props.addGuestName} />
  </header>

Header.propTypes = {
  submitGuest: PropTypes.func.isRequired,
  pendingGuest: PropTypes.string.isRequired,
  addGuestName: PropTypes.func.isRequired
}

export default Header;