import React from 'react';
import PropTypes from 'prop-types';

const GuestInputForm = props => 
  <form onSubmit={props.submitGuest}>
    <input 
      type="text" 
      value={props.pendingGuest}
      onChange={e => props.addGuestName(e.target.value)}
      placeholder="Invite Someone" />
    <button type="submit" name="submit" value="submit">Submit</button>
  </form>

GuestInputForm.propTypes = {
  submitGuest: PropTypes.func.isRequired,
  pendingGuest: PropTypes.string.isRequired,
  addGuestName: PropTypes.func.isRequired
}

export default GuestInputForm;